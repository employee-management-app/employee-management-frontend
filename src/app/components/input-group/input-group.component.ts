import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OutSideClickDirective } from 'src/app/directives/outside-click.directive';

@Component({
  selector: 'app-input-group',
  standalone: true,
  imports: [
    CommonModule,
    OutSideClickDirective,
  ],
  templateUrl: './input-group.component.html',
  styleUrls: ['./input-group.component.css']
})
export class InputGroupComponent implements OnInit {
  @Input() id!: string;
  @Input() label!: string;
  @Input() isRequiredFlag!: boolean;
  @Input() group!: string;
  @Output() selectedOption!: any;
  @Output() selectedOptionId: EventEmitter<any> = new EventEmitter();

  isOpen: boolean = false;
  searchText: string = '';
  filteredOptions: Array<any> = new Array();
  options = [
    {
      id: 'choose_group',
      label: 'Choose group...'
    },
    {
      id: 'Marketing',
      label: 'Marketing'
    },
    {
      id: 'Bussiness Development',
      label: 'Bussiness Development'
    },
    {
      id: 'Software Developer',
      label: 'Software Developer'
    },
    {
      id: 'Community',
      label: 'Community'
    },
    {
      id: 'Human Resource',
      label: 'Human Resource'
    },
    {
      id: 'Operations',
      label: 'Operations'
    },
    {
      id: 'Product',
      label: 'Product'
    },
    {
      id: 'Quality Assurance',
      label: 'Quality Assurance'
    },
    {
      id: 'Payment',
      label: 'Payment'
    },
    {
      id: 'Audit',
      label: 'Audit'
    },
  ];

  ngOnInit(): void {
    this.filteredOptions = this.options;

    this.selectedOption = {
      id: 'choose_group',
      label: 'Choose group...'
    };
    this.onSelectOption(this.selectedOption);

    if (this.group) {
      const findGroupId = this.findGroup(this.group)?.id;
      if (this.group === findGroupId) {
        this.onSelectOption(this.findGroup(this.group));
      }
    }

    this.isOpen = false;
  }

  toggle() {
    this.isOpen = !this.isOpen;
  }

  onSelectOption(value: any) {
    this.selectedOption = value;
    this.selectedOptionId.emit(value && value.id || null);
    this.toggle();
  }

  onChange(event: any) {
    if (event && event.target && event.target.value !== '') {
      const filteredOptions = this.options.filter((data) => data && data.label.toLocaleLowerCase().includes(event.target.value.toLocaleLowerCase()));
      this.filteredOptions = filteredOptions;
    } else {
      this.filteredOptions = this.options;
    }
  }

  findGroup(group: string) {
    return this.filteredOptions.find((data) => (data && data.id) && (this.removeSpaceFromString(data.id).toLocaleLowerCase() === this.removeSpaceFromString(group).toLocaleLowerCase()));
  }

  removeSpaceFromString(data: string) {
    if (typeof data !== 'string') throw new Error('Data must be string');
    return data.replace(/\s/g, '');
  }
}
