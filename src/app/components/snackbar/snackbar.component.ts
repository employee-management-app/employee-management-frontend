import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-snackbar',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.css']
})
export class SnackbarComponent {
  @Input() message!: string;
  @Input() type!: string;
  @Output() actionClose: EventEmitter<void> = new EventEmitter(); 

  onClose() {
    this.actionClose.emit();
  }
}
