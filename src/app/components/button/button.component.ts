import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  standalone: true,
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css'],
  imports: [CommonModule]
})
export class ButtonComponent {
  @Input() type: string = 'button';
  @Input() label: string = 'label';
  @Input() class: string = 'w-full h-full min-h-44px max-h-44px rounded-8px shadow-elevation1 bg-primary text-white font-semibold text-f12-l18 line-clamp-1 whitespace-nowrap flex items-center justify-center gap-2 px-4';
  @Input() isLoading: boolean = false;
  @Input() disabled: boolean = false;
  @Output() action = new EventEmitter<any>();

  onClick() {
    this.action.emit();
  }
}
