import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button-with-icon',
  standalone: true,
  templateUrl: './button-with-icon.component.html',
  styleUrls: ['./button-with-icon.component.css']
})
export class ButtonWithIconComponent {
  @Input() type: string = 'button';
  @Input() label: string = 'label';
  @Input() iconUrl: string = '/assets/img/svg/add-white-24.svg';
  @Input() class: string = 'w-full h-full min-h-44px max-h-44px rounded-8px bg-primary shadow-elevation1 px-4 flex items-center justify-center gap-5px overflow-hidden';
  @Output() action = new EventEmitter<any>();

  onClick() {
    this.action.emit();
  }
}
