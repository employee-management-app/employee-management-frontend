import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { OutSideClickDirective } from 'src/app/directives/outside-click.directive';

@Component({
  selector: 'app-basic-salary-sort',
  standalone: true,
  imports: [
    CommonModule,
    OutSideClickDirective
  ],
  templateUrl: './basic-salary-sort.component.html',
  styleUrls: ['./basic-salary-sort.component.css']
})
export class BasicSalarySortComponent implements OnInit {
  @Input() label!: string;
  @Input() sort!: any;
  @Output() selectedOption!: any;
  @Output() selectedOptionId: EventEmitter<any> = new EventEmitter();

  options = [
    {
      id: null,
      label: 'Choose sort...'
    },
    {
      id: 'salary-asc',
      label: 'Ascending'
    },
    {
      id: 'salary-desc',
      label: 'Descending'
    },
  ];

  isOpen: boolean = false;

  ngOnInit(): void {
    if (this.sort === 'salary-asc') {
      this.selectedOption = {
        id: this.sort,
        label: 'Ascending'
      };
      this.onSelectOption(this.selectedOption);
    } else if (this.sort === 'salary-desc') {
      this.selectedOption = {
        id: this.sort,
        label: 'Descending'
      };
      this.onSelectOption(this.selectedOption);
    } else {
      this.selectedOption = {
        id: '',
        label: 'Choose sort...'
      };
      this.onSelectOption(this.selectedOption);
    }

    this.toggle();
  }

  toggle() {
    this.isOpen = !this.isOpen;
  }

  onSelectOption(value: any) {
    this.selectedOption = value;
    this.selectedOptionId.emit(value && value.id || '');
    this.toggle();
  }
}
