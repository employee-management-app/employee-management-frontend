import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ButtonComponent } from '../button/button.component';

@Component({
  selector: 'app-confirmation-modal-box',
  standalone: true,
  templateUrl: './confirmation-modal-box.component.html',
  styleUrls: ['./confirmation-modal-box.component.css'],
  imports: [
    ButtonComponent
  ]
})
export class ConfirmationModalBoxComponent {
  @Input() message!:string;
  @Input() cancelButtonLabel!: string;
  @Input() confirmButtonLabel!: string;
  @Input() isLoadingConfirmAction: boolean = false;
  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() confirm: EventEmitter<any> = new EventEmitter();

  onCancel() {
    this.cancel.emit();
  }

  onConfirm() {
    this.confirm.emit();
  }
}
