import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-table-loading',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './table-loading.component.html',
  styleUrls: ['./table-loading.component.css']
})
export class TableLoadingComponent {
  @Input({ required: true }) colspan: number = 1;
}
