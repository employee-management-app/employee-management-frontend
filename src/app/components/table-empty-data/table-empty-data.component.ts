import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-table-empty-data',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './table-empty-data.component.html',
  styleUrls: ['./table-empty-data.component.css']
})
export class TableEmptyDataComponent {

}
