import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-status-label',
  standalone: true,
  templateUrl: './status-label.component.html',
  styleUrls: ['./status-label.component.css'],
  imports: [
    CommonModule
  ]
})
export class StatusLabelComponent {
  @Input({ required: true }) status: string = 'Active';
}
