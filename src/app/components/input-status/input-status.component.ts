import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OutSideClickDirective } from 'src/app/directives/outside-click.directive';

@Component({
  selector: 'app-input-status',
  standalone: true,
  imports: [
    CommonModule,
    OutSideClickDirective
  ],
  templateUrl: './input-status.component.html',
  styleUrls: ['./input-status.component.css']
})
export class InputStatusComponent implements OnInit {
  @Input() id!: string;
  @Input() label!: string;
  @Input() isRequiredFlag!: boolean;
  @Input() status!: any;
  @Output() selectedOption!: any;
  @Output() selectedOptionId: EventEmitter<any> = new EventEmitter();

  options = [
    {
      id: 'choose_status',
      label: 'Choose status...'
    },
    {
      id: 1,
      label: 'Active'
    },
    {
      id: 2,
      label: 'Inactive'
    },
  ];

  isOpen: boolean = false;
  
  ngOnInit(): void {
    this.selectedOption = {
      id: 'choose_status',
      label: 'Choose status...'
    };
    this.onSelectOption(this.selectedOption);

    if (this.status) {
      const findStatusLabel = this.findStatus(this.status)?.label;
      if (this.status === findStatusLabel) {
        this.onSelectOption(this.findStatus(this.status));
      }
    }

    this.isOpen = false;
  }

  toggle() {
    this.isOpen = !this.isOpen;
  }

  onSelectOption(value: any) {
    this.selectedOption = value;
    this.selectedOptionId.emit(value && value.id || '');
    this.toggle();
  }

  findStatus(status: string) {
    return this.options.find((data) => (data && data.label) && (this.removeSpaceFromString(data.label).toLocaleLowerCase() === this.removeSpaceFromString(status).toLocaleLowerCase()));
  }

  removeSpaceFromString(data: string) {
    if (typeof data !== 'string') throw new Error('Data must be string');
    return data.replace(/\s/g, '');
  }
}
