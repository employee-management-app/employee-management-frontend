import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-pagination',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  @Input() startPage: number = 1;
  @Input() totalData: number = 0;
  @Input() limitList!: Array<number>;
  @Input() limitPage: number = 10;
  @Output() changePage: EventEmitter<any> = new EventEmitter();
  @Output() changeLimit: EventEmitter<any> = new EventEmitter();

  control: FormControl<number> = new FormControl();

  firstInPage: number = 1;
  lastInPage: number = 10;
  disabledFirstPageButton!: boolean;
  disabledLastPageButton!: boolean;

  ngOnInit(): void {
    this.limitList = [10, 50, 100, 200, 500];
    this.control.setValue(this.limitPage);
    this.initData();
  }

  initData() {
    this.firstInPage = this.startPage * this.limitPage - (this.limitPage - 1);
    this.lastInPage = this.totalData > this.startPage * this.limitPage ? this.startPage * this.limitPage : this.totalData;
    this.disabledFirstPageButton = Boolean(this.totalData === 0 || this.startPage === 1);
    this.disabledLastPageButton = Boolean(this.totalData === 0 || (this.startPage === Math.ceil(this.totalData / this.limitPage)));
  }

  firstPage() {
    this.startPage = 1;
    this.onChangePage(this.startPage);
  }

  lastPage() {
    this.startPage = Math.ceil(this.totalData / this.limitPage);
    this.onChangePage(this.startPage);
  }

  toPage(page: number) {
    this.startPage += page;
    console.log(this.startPage);
    this.onChangePage(this.startPage);
  }

  onChangePage(page: number) {
    this.initData();
    this.changePage.emit(page);
  }
  
  onChangeLimit() {
    this.initData();
    this.changeLimit.emit(this.control.value);
  }
}
