import { NgModule, importProvidersFrom } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonComponent } from 'src/app/components/button/button.component';
import { HttpClientModule } from '@angular/common/http';
import { SnackbarComponent } from 'src/app/components/snackbar/snackbar.component';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,
    ButtonComponent,
    SnackbarComponent
  ],
  providers: [
    importProvidersFrom(HttpClientModule)
  ]
})
export class AuthModule { }
