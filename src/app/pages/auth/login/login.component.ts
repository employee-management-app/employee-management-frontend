import { Component, OnInit, inject } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  router = inject(Router);
  authService = inject(AuthService);
  fb = inject(FormBuilder);

  showPassword: boolean = false;
  loginLoading: boolean = false;
  showSnackbar: boolean = false;
  snackbarType!: string;
  snackbarMessage!: string;

  loginForm!: FormGroup;

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword
  }

  onSubmit() {
    this.loginLoading = true;
    this.authService.loginAdminUserService(this.loginForm.value).subscribe({
      next: (res) => {
        if (res && res.status === 'OK' && res.accessToken) {
          this.authService.setSession(res.accessToken);
          this.router.navigate(['/']);
          this.loginLoading = false;
        }
      },
      error: (error) => {
        this.snackbarType =  'danger';
        this.snackbarMessage = error && error.error && error.error.message || null;
        this.showSnackbar = true;
        this.loginLoading = false;
      }
    });
  }
}
