import { DateTime } from 'luxon';
import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { ButtonComponent } from 'src/app/components/button/button.component';
import { InputStatusComponent } from 'src/app/components/input-status/input-status.component';
import { InputGroupComponent } from 'src/app/components/input-group/input-group.component';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { EmployeeService } from 'src/app/services/employee.service';
import { SnackbarComponent } from 'src/app/components/snackbar/snackbar.component';

@Component({
  selector: 'app-edit-employee',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    ButtonComponent,
    InputStatusComponent,
    InputGroupComponent,
    ReactiveFormsModule,
    SnackbarComponent
  ],
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {
  router = inject(Router);
  fb = inject(FormBuilder);
  employeeService = inject(EmployeeService);
  activatedRoute = inject(ActivatedRoute);

  form!: FormGroup;

  today = DateTime.now().toFormat('yyyy-MM-dd');
  employeeData!: any;
  isLoading: boolean = false;
  editEmployeeLoading: boolean = false;
  showSnackbar: boolean = false;
  snackbarType!: string;
  snackbarMessage!: string;

  ngOnInit(): void {
    this.isLoading = true;

    this.form = this.fb.group({
      username: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birthDate: ['', Validators.required],
      basicSalary: ['', [Validators.required, Validators.pattern(/^[1-9]\d*(\.\d+)?$/)]],
      status: ['', Validators.required],
      group: ['', Validators.required],
      description: ['', Validators.required]
    });

    const employeeId = this.activatedRoute.snapshot.paramMap.get('employee_id');
    this.employeeService.employeeDetail(employeeId).subscribe({
      next: (res) => {
        if (res && res.status === 'OK' && res.result) {
          const result = res.result || null;
          this.employeeData = result;
          this.fillForm();
          this.isLoading = false;
        }
      }
    });
  }

  fillForm() {
    this.form.reset();
    this.form.setValue({
      username: this.employeeData.username,
      firstName: this.employeeData.firstName,
      lastName: this.employeeData.lastName,
      email: this.employeeData.email,
      birthDate: this.employeeData.birthDate,
      basicSalary: this.employeeData.basicSalary,
      status: this.employeeData.status,
      group: this.employeeData.group,
      description: this.employeeData.description
    })
  }

  cancelEditEmployee() {
    this.router.navigate(['/']);
  }

  onSelectedStatus(statusId: any) {
    this.form.get('status')?.markAsDirty();
    this.form.get('status')?.setValue(statusId);
  }

  onSelectedGroup(groupId: any) {
    this.form.get('group')?.markAsDirty();
    this.form.get('group')?.setValue(groupId);
  }

  toggleShowSnackbar() {
    this.showSnackbar = !this.showSnackbar;
  }

  autoCloseSnackbar() {
    setTimeout(() => {
      this.showSnackbar = false;
    }, 5000);
  }

  onSubmit() {
    this.editEmployeeLoading = true;
    const employeeId = this.activatedRoute.snapshot.paramMap.get('employee_id');
    this.employeeService.editEmployee(employeeId, this.form.value).subscribe({
      next: (res) => {
        if (res && res.status === 'OK' && res.result) {
          this.form.reset();
          this.snackbarType = 'warning';
          this.snackbarMessage = 'Employee data updated successfully';
          this.showSnackbar = true;
          this.autoCloseSnackbar();
          setTimeout(() => {
            this.editEmployeeLoading = false;
            this.router.navigate(['/']);
          }, 2000);
        }
      },
      error: (err) => {
        this.editEmployeeLoading = false;
        this.snackbarType = 'danger';
        this.snackbarMessage = err && err.error && err.error.message || null;
        this.showSnackbar = true;
        this.autoCloseSnackbar();
      }
    });
  }
}
