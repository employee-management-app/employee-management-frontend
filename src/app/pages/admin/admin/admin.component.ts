import { Component, inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent {
  router = inject(Router);
  authService = inject(AuthService);

  showSideBar: boolean = false;
  showLogoutConfirmationModalBox: boolean = false
  logoutLoading: boolean = false;
  showSnackbar: boolean = false;
  snackbarType!: string;
  snackbarMessage!: string;

  toggleSideBar() {
    this.showSideBar = !this.showSideBar;

    if (this.showSideBar) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'unset';
    }
  }

  toggleLogoutConfirmationModalBox() {
    this.showLogoutConfirmationModalBox = !this.showLogoutConfirmationModalBox;
  }

  toggleShowSnackbar() {
    this.showSnackbar = !this.showSnackbar
  }

  logout() {
    this.logoutLoading = true;
    this.authService.logout().subscribe({
      next: (res) => {
        if (res && res.status === 'OK') {
          this.authService.logout();
          this.authService.removeSession();
          this.logoutLoading = false;
          this.showLogoutConfirmationModalBox = false;
          this.router.navigate(['/login']);
        }
      },
      error: (err) => {
        this.logoutLoading = false;
        this.showSnackbar = true;
        this.snackbarType = 'danger';
        this.snackbarMessage = err && err.error && err.error.message || null;
      }
    })
  }

  navigateTo(url: string) {
    this.toggleSideBar();
    this.router.navigate([url]);
  }
}
