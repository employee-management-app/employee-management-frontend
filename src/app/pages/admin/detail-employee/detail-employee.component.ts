import { DateTime } from 'luxon';
import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ButtonComponent } from 'src/app/components/button/button.component';
import { StatusLabelComponent } from 'src/app/components/status-label/status-label.component';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-detail-employee',
  standalone: true,
  templateUrl: './detail-employee.component.html',
  styleUrls: ['./detail-employee.component.css'],
  imports: [
    CommonModule,
    StatusLabelComponent,
    ButtonComponent
  ]

})
export class DetailEmployeeComponent implements OnInit {
  router = inject(Router);
  employeeService = inject(EmployeeService);
  activatedRoute = inject(ActivatedRoute)

  isLoading: boolean = false;
  employeeData!: any;
  showSnackbar: boolean = false;
  snackbarType!: string;
  snackbarMessage!: string;

  ngOnInit(): void {
    this.isLoading = true;

    const employeeId = this.activatedRoute.snapshot.paramMap.get('employee_id');
    this.employeeService.employeeDetail(employeeId).subscribe({
      next: (res) => {
        if (res && res.status === 'OK' && res.result) {
          const result = res.result || null;
          this.employeeData = result;
          this.isLoading = false;
        }
      },
      error: (err) => {
        this.isLoading = false;
        this.snackbarType = 'danger';
        this.snackbarMessage = err && err.error && err.error.message || null;
        this.showSnackbar = true;
        this.autoCloseSnackbar();
      }
    });
  }

  toggleShowSnackbar() {
    this.showSnackbar = !this.showSnackbar;
  }

  autoCloseSnackbar() {
    setTimeout(() => {
      this.showSnackbar = false;
    }, 5000);
  }

  backToDashboard() {
    this.router.navigate(['/']);
  }

  salaryCurrencyFormatter(data: number) {
    const salary = new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
    }).format(data)
    return `${salary},-`
  }

  dateFormatter(data: string) {
    return DateTime.fromISO(new Date(data).toISOString()).toFormat('dd MMMM yyyy')
  }
}
