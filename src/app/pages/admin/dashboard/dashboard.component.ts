import { DateTime } from 'luxon';
import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { BasicSalarySortComponent } from 'src/app/components/basic-salary-sort/basic-salary-sort.component';
import { ButtonWithIconComponent } from 'src/app/components/button-with-icon/button-with-icon.component';
import { ButtonComponent } from 'src/app/components/button/button.component';
import { ConfirmationModalBoxComponent } from 'src/app/components/confirmation-modal-box/confirmation-modal-box.component';
import { PaginationComponent } from 'src/app/components/pagination/pagination.component';
import { SnackbarComponent } from 'src/app/components/snackbar/snackbar.component';
import { StatusLabelComponent } from 'src/app/components/status-label/status-label.component';
import { TableEmptyDataComponent } from 'src/app/components/table-empty-data/table-empty-data.component';
import { TableLoadingComponent } from 'src/app/components/table-loading/table-loading.component';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-dashboard',
  standalone: true,
  templateUrl: './dashboard.component.html',
  imports: [
    CommonModule,
    ButtonComponent,
    ButtonWithIconComponent,
    PaginationComponent,
    StatusLabelComponent,
    ConfirmationModalBoxComponent,
    SnackbarComponent,
    TableLoadingComponent,
    TableEmptyDataComponent,
    BasicSalarySortComponent,
    ReactiveFormsModule
  ]
})
export class DashboardComponent implements OnInit {
  router = inject(Router);
  employeeService = inject(EmployeeService);
  fb = inject(FormBuilder);

  showConfirmationDeleteEmployeeModalBox: boolean = false;
  selectedEmployeeId!: string;
  isLoading: boolean = false;
  showSnackbar: boolean = false;
  snackbarType!: string;
  snackbarMessage!: string;
  tableLoading: boolean = false;
  deleteEmployeeLoading: boolean = false;

  meta: any = {
    page: 1,
    limit: 10,
    username: '',
    firstName: '',
    lastName: '',
    email: '',
    birthDate: '',
    basicSalary: '',
    status: '',
    group: '',
    sortBy: ''
  };

  employeeData: any = {
    docs: [],
    totalDocs: 0,
    page: 1,
    limit: 10
  };

  searchEmployeeByNameForm!: FormGroup;
  searchEmployeeByEmail!: FormGroup;

  ngOnInit(): void {
    this.isLoading = true;

    this.searchEmployeeByNameForm = this.fb.group({
      employeeName: ['']
    });

    this.searchEmployeeByEmail = this.fb.group({
      employeeEmail: ['']
    });

    this.getEmployeeFilterHistory();
    this.employeeService.employeeList(this.meta).subscribe({
      next: (res) => {
        if (res && res.status === 'OK' && res.result) {
          const result = res.result || null;
          this.employeeData = result;
          this.isLoading = false;
          this.tableLoading = false
        }
      },
      error: (err) => {
        this.isLoading = false;
        this.tableLoading = false
        this.snackbarType = 'danger';
        this.snackbarMessage = err && err.error && err.error.message || null;
        this.showSnackbar = true;
        this.autoCloseSnackbar();
      }
    });
  }

  onClickAddEmployee() {
    this.router.navigate(['/add-employee']);
  }

  onClickViewDetailEmployee(employeeId: string) {
    this.router.navigate(['/detail-employee', employeeId]);
  }

  onClickEditEmployee(employeeId: string) {
    this.router.navigate(['/edit-employee', employeeId]);
  }

  onClickDeleteEmployee(employeeId: string) {
    this.selectedEmployeeId = employeeId;
    this.showConfirmationDeleteEmployeeModalBox = true;
  }

  closeConfirmationDeleteEmployeeModalBox() {
    this.selectedEmployeeId = '';
    this.showConfirmationDeleteEmployeeModalBox = false;
  }

  confirmDeleteEmployee() {
    this.deleteEmployeeLoading = true;
    this.employeeService.deleteEmploye(this.selectedEmployeeId).subscribe({
      next: (res) => {
        console.log('res: ', res);
        if (res && res.status === 'OK') {
          setTimeout(() => {
            this.closeConfirmationDeleteEmployeeModalBox();
            this.snackbarType = 'danger';
            this.snackbarMessage = 'Employee data deleted successfully';
            this.showSnackbar = true;
            this.autoCloseSnackbar();
            this.deleteEmployeeLoading = false;

            this.tableLoading = true;
            this.employeeService.employeeList(this.meta).subscribe({
              next: (res) => {
                if (res && res.status === 'OK' && res.result) {
                  const result = res.result || null;
                  this.employeeData = result;
                  this.isLoading = false;
                  this.tableLoading = false
                } 
              },
              error: (err) => {
                this.isLoading = false;
                this.tableLoading = false
                this.snackbarType = 'danger';
                this.snackbarMessage = err && err.error && err.error.message || null;
                this.showSnackbar = true;
                this.autoCloseSnackbar();
              }
            });
          }, 500);
        }
      },
      error: (err) => {
        this.isLoading = false;
        this.tableLoading = false
        this.snackbarType = 'danger';
        this.snackbarMessage = err && err.error && err.error.message || null;
        this.showSnackbar = true;
        this.autoCloseSnackbar();
      }
    });
  }

  toggleShowSnackbar() {
    this.showSnackbar = !this.showSnackbar;
  }

  autoCloseSnackbar() {
    setTimeout(() => {
      this.showSnackbar = false;
    }, 5000);
  }

  resetEmployeeData() {
    this.employeeData.docs = [];
    this.employeeData.totalDocs = 0;
    this.employeeData.page = 1;
    this.employeeData.limit = 10;
  }

  changePage(value: any) {
    this.tableLoading = true;
    this.meta = {
      ...this.meta,
      page: value
    };

    this.employeeService.employeeList(this.meta).subscribe({
      next: (res) => {
        if (res && res.status === 'OK' && res.result) {
          const result = res.result || null;
          this.employeeData = result;
          this.setEmployeeFilterHistory();
          this.isLoading = false;
          this.tableLoading = false
        } 
      },
      error: (err) => {
        this.isLoading = false;
        this.tableLoading = false
        this.snackbarType = 'danger';
        this.snackbarMessage = err && err.error && err.error.message || null;
        this.showSnackbar = true;
        this.autoCloseSnackbar();
      }
    });
  }
  
  changeLimit(value: any) {
    this.tableLoading = true;
    this.resetEmployeeData();
    
    this.meta = {
      ...this.meta,
      page: 1,
      limit: value
    };
    
    this.employeeService.employeeList(this.meta).subscribe({
      next: (res) => {
        if (res && res.status === 'OK' && res.result) {
          const result = res.result || null;
          this.employeeData = result;
          this.setEmployeeFilterHistory();
          this.isLoading = false;
          this.tableLoading = false
        } 
      },
      error: (err) => {
        this.isLoading = false;
        this.tableLoading = false
        this.snackbarType = 'danger';
        this.snackbarMessage = err && err.error && err.error.message || null;
        this.showSnackbar = true;
        this.autoCloseSnackbar();
      }
    });
  }

  onSortEmployeeDataList(sort: string) {
    this.tableLoading = true;
    this.resetEmployeeData();

    this.meta = {
      ...this.meta,
      sortBy: sort
    };

    this.employeeService.employeeList(this.meta).subscribe({
      next: (res) => {
        if (res && res.status === 'OK' && res.result) {
          const result = res.result || null;
          this.employeeData = result;
          this.setEmployeeFilterHistory();
          this.isLoading = false;
          this.tableLoading = false
        } 
      },
      error: (err) => {
        this.isLoading = false;
        this.tableLoading = false
        this.snackbarType = 'danger';
        this.snackbarMessage = err && err.error && err.error.message || null;
        this.showSnackbar = true;
        this.autoCloseSnackbar();
      }
    });
  }

  onFilterEmployeeByName() {
    this.tableLoading = true;
    this.resetEmployeeData();

    this.meta = {
      ...this.meta,
      firstName: this.searchEmployeeByNameForm.get('employeeName')?.value
    };

    this.employeeService.employeeList(this.meta).subscribe({
      next: (res) => {
        if (res && res.status === 'OK' && res.result) {
          const result = res.result || null;
          this.employeeData = result;
          this.setEmployeeFilterHistory();
          this.isLoading = false;
          this.tableLoading = false
        } 
      },
      error: (err) => {
        this.isLoading = false;
        this.tableLoading = false
        this.snackbarType = 'danger';
        this.snackbarMessage = err && err.error && err.error.message || null;
        this.showSnackbar = true;
        this.autoCloseSnackbar();
      }
    });
  }

  onFilterEmployeeByEmail() {
    this.tableLoading = true;
    this.resetEmployeeData();

    this.meta = {
      ...this.meta,
      email: this.searchEmployeeByEmail.get('employeeEmail')?.value
    };

    this.employeeService.employeeList(this.meta).subscribe({
      next: (res) => {
        if (res && res.status === 'OK' && res.result) {
          const result = res.result || null;
          this.employeeData = result;
          this.setEmployeeFilterHistory();
          this.isLoading = false;
          this.tableLoading = false
        } 
      },
      error: (err) => {
        this.isLoading = false;
        this.tableLoading = false
        this.snackbarType = 'danger';
        this.snackbarMessage = err && err.error && err.error.message || null;
        this.showSnackbar = true;
        this.autoCloseSnackbar();
      }
    });
  }

  salaryCurrencyFormatter(data: number) {
    const salary = new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
    }).format(data)
    return `${salary},-`
  }

  dateFormatter(data: string) {
    return DateTime.fromISO(new Date(data).toISOString()).toFormat('dd MMMM yyyy')
  }

  onChangeFilterEmployeeByName() {
    if (!this.searchEmployeeByNameForm.get('employeeName')?.value) {
      this.onFilterEmployeeByName();
    }
  }

  onChangeFilterEmployeeByEmail() {
    if (!this.searchEmployeeByEmail.get('employeeEmail')?.value) {
      this.onFilterEmployeeByEmail();
    }
  }

  getEmployeeFilterHistory() {
    const employeeFilterHistory = localStorage.getItem('employeeFilterHistory');
    const employeeFilterHistoryJson = employeeFilterHistory ? JSON.parse(employeeFilterHistory) : null;

    if (employeeFilterHistoryJson?.page) {
      this.meta.page = employeeFilterHistoryJson.page;
    }

    if (employeeFilterHistoryJson?.limit) {
      this.meta.limit = employeeFilterHistoryJson.limit;
    }

    if (employeeFilterHistoryJson?.username) {
      this.meta.username = employeeFilterHistoryJson.username;
    }

    if (employeeFilterHistoryJson?.firstName) {
      this.meta.firstName = employeeFilterHistoryJson.firstName;
      this.searchEmployeeByNameForm.get('employeeName')?.setValue(employeeFilterHistoryJson.firstName);
    }

    if (employeeFilterHistoryJson?.lastName) {
      this.meta.lastName = employeeFilterHistoryJson.lastName;
    }

    if (employeeFilterHistoryJson?.email) {
      this.meta.email = employeeFilterHistoryJson.email;
      this.searchEmployeeByEmail.get('employeeEmail')?.setValue(employeeFilterHistoryJson.email);
    }

    if (employeeFilterHistoryJson?.birthDate) {
      this.meta.birthDate = employeeFilterHistoryJson.birthDate;
    }

    if (employeeFilterHistoryJson?.basicSalary) {
      this.meta.basicSalary = employeeFilterHistoryJson.basicSalary;
    }

    if (employeeFilterHistoryJson?.status) {
      this.meta.status = employeeFilterHistoryJson.status;
    }

    if (employeeFilterHistoryJson?.group) {
      this.meta.group = employeeFilterHistoryJson.group;
    }

    if (employeeFilterHistoryJson?.sortBy) {
      this.meta.sortBy = employeeFilterHistoryJson.sortBy;
    }
  }

  setEmployeeFilterHistory() {
    const employeeFilterHistory = localStorage.getItem('employeeFilterHistory');
    const employeeFilterHistoryJson = employeeFilterHistory ? JSON.parse(employeeFilterHistory) : null;

    if (employeeFilterHistoryJson?.page !== this.meta.page) {
      localStorage.setItem('employeeFilterHistory', JSON.stringify(this.meta));
    }

    if (employeeFilterHistoryJson?.limit !== this.meta.limit) {
      localStorage.setItem('employeeFilterHistory', JSON.stringify(this.meta));
    }

    if (employeeFilterHistoryJson?.username !== this.meta.username) {
      localStorage.setItem('employeeFilterHistory', JSON.stringify(this.meta));
    }

    if (employeeFilterHistoryJson?.firstName !== this.meta.firstName) {
      localStorage.setItem('employeeFilterHistory', JSON.stringify(this.meta));
    }

    if (employeeFilterHistoryJson?.lastName !== this.meta.lastName) {
      localStorage.setItem('employeeFilterHistory', JSON.stringify(this.meta));
    }

    if (employeeFilterHistoryJson?.email !== this.meta.email) {
      localStorage.setItem('employeeFilterHistory', JSON.stringify(this.meta));
    }

    if (employeeFilterHistoryJson?.birthDate !== this.meta.birthDate) {
      localStorage.setItem('employeeFilterHistory', JSON.stringify(this.meta));
    }

    if (employeeFilterHistoryJson?.basicSalary !== this.meta.basicSalary) {
      localStorage.setItem('employeeFilterHistory', JSON.stringify(this.meta));
    }

    if (employeeFilterHistoryJson?.status !== this.meta.status) {
      localStorage.setItem('employeeFilterHistory', JSON.stringify(this.meta));
    }

    if (employeeFilterHistoryJson?.group !== this.meta.group) {
      localStorage.setItem('employeeFilterHistory', JSON.stringify(this.meta));
    }

    if (employeeFilterHistoryJson?.sortBy !== this.meta.sortBy) {
      localStorage.setItem('employeeFilterHistory', JSON.stringify(this.meta));
    }
  }
}
