import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin/admin.component';
import { ConfirmationModalBoxComponent } from 'src/app/components/confirmation-modal-box/confirmation-modal-box.component';
import { SnackbarComponent } from 'src/app/components/snackbar/snackbar.component';

@NgModule({
  declarations: [
    AdminComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ConfirmationModalBoxComponent,
    SnackbarComponent,
  ]
})
export class AdminModule { }
