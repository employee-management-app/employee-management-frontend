import { DateTime } from 'luxon';
import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { ButtonComponent } from 'src/app/components/button/button.component';
import { InputStatusComponent } from 'src/app/components/input-status/input-status.component';
import { InputGroupComponent } from 'src/app/components/input-group/input-group.component';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { EmployeeService } from 'src/app/services/employee.service';
import { SnackbarComponent } from 'src/app/components/snackbar/snackbar.component';

@Component({
  selector: 'app-add-employee',
  standalone: true,
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css'],
  imports: [
    CommonModule,
    RouterLink,
    ButtonComponent,
    InputStatusComponent,
    InputGroupComponent,
    ReactiveFormsModule,
    SnackbarComponent,
  ]
})
export class AddEmployeeComponent implements OnInit {
  router = inject(Router);
  employeeService = inject(EmployeeService);
  fb = inject(FormBuilder);

  form!: FormGroup;

  today = DateTime.now().toFormat('yyyy-MM-dd');
  addEmployeeLoading: boolean = false;
  showSnackbar: boolean = false;
  snackbarType!: string;
  snackbarMessage!: string;
  isLoading: boolean = false;

  ngOnInit(): void {
    this.isLoading = true;
    this.form = this.fb.group({
      username: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birthDate: ['', Validators.required],
      basicSalary: ['', [Validators.required, Validators.pattern(/^[1-9]\d*(\.\d+)?$/)]],
      status: ['', Validators.required],
      group: ['', Validators.required],
      description: ['', Validators.required]
    });

    this.isLoading = false;
  }

  cancelAddEmployee() {
    this.router.navigate(['/']);
  }

  onSelectedStatus(statusId: any) {
    this.form.get('status')?.markAsDirty();
    this.form.get('status')?.setValue(statusId);
  }
  
  onSelectedGroup(groupId: any) {
    this.form.get('group')?.markAsDirty();
    this.form.get('group')?.setValue(groupId);
  }

  toggleShowSnackbar() {
    this.showSnackbar = !this.showSnackbar;
  }

  autoCloseSnackbar() {
    setTimeout(() => {
      this.showSnackbar = false;
    }, 5000);
  }

  onSubmit() {
    this.addEmployeeLoading = true;
    this.employeeService.addEmployee(this.form.value).subscribe({
      next: (res) => {
        if (res && res.status === 'OK') {
          this.addEmployeeLoading = false;
          this.form.reset();
          this.router.navigate(['/']);
        }
      },
      error: (err) => {
        this.addEmployeeLoading = false;
        this.snackbarType = 'danger';
        this.snackbarMessage = err && err.error && err.error.message || null;
        this.showSnackbar = true;
        this.autoCloseSnackbar();
      }
    })
  }
}
