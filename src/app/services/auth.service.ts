import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  http = inject(HttpClient);

  baseUrl: string = 'http://localhost:5000/api/auth';

  loginAdminUserService(payload: any) {
    return this.http.post<any>(`${this.baseUrl}/login-admin-user`, payload);
  }

  logout() {
    return this.http.post<any>(`${this.baseUrl}/logout`, {});
  }

  setSession(accessToken: string) {
    const expiresAt = moment().add(2592000, 'second');
    localStorage.setItem('accessToken', `Bearer ${accessToken}`);
    localStorage.setItem('expiresIn', JSON.stringify(expiresAt));
  }

  removeSession() {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('expiresIn');
    localStorage.removeItem('employeeFilterHistory');
  }

  getExpiration() {
    const expiration = localStorage.getItem("expiresIn");
    const expirationAt = JSON.parse(expiration || '{}');
    return moment(expirationAt);
  }

  public isLoggedIn() {
    const accessToken = localStorage.getItem('accessToken');
    return Boolean(accessToken && moment().isBefore(this.getExpiration()));
  }
}
