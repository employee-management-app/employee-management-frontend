import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  http = inject(HttpClient);

  baseUrl: string = 'http://localhost:5000/api/employee';

  addEmployee(payload: any) {
    return this.http.post<any>(`${this.baseUrl}/add`, payload);
  }

  employeeList(params: any) {
    const _params = {}

    if (typeof params === 'object') {
      Object.assign(_params, params)
    }
    
    return this.http.get<any>(`${this.baseUrl}/list`, { params: _params });
  }

  employeeDetail(employeeId: any) {
    return this.http.get<any>(`${this.baseUrl}/detail/${employeeId}`);
  }

  editEmployee(employeeId: any, payload: any) {
    return this.http.put<any>(`${this.baseUrl}/edit/${employeeId}`, payload);
  }

  deleteEmploye(employeeId: any) {
    return this.http.delete<any>(`${this.baseUrl}/delete/${employeeId}`);
  }
}
