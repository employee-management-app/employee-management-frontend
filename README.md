# EMPLOYEE MANAGEMENT API
This project was created with [Angular@16](https://v16.angular.io/docs)

## Table of Contents
* [How to Setup](#how-to-setup)
* [How to Run](#how-to-run)
* [Libraries](#libraries)
* [Must to Know](#must-to-know)

## How to Setup
* Setup the Backend
  - Before running this application you must ensure that the backend application is running.
  - You can see how to run the backend application via the following [link](https://gitlab.com/employee-management-app/employee-management-api#how-to-setup)
* Install nvm to install npm and node with management version
* Install the package
  - ` $ npm install `

## How to Run
Run ` $ npm start ` to run the application. Make sure the backend application is running on port 5000, because this application is connected to http://localhost:5000

## Libraries
* CSS Framework [tailwind](https://tailwindcss.com/)

## Must to Know
* Before testing this application, please register and log in first using the following [API](https://web.postman.co/workspace/My-Workspace~768e55ed-ecbf-491b-88cb-a92754d74ccf/folder/7899277-388f5dc1-dae7-4b88-8624-f71c5b78b87d?action=share&source=copy-link&creator=7899277&active-environment=fefbbb19-4ee0-493f-aa17-1a8f397e41fd)
* If you want to inject dummy data, you can use the following [API](https://web.postman.co/workspace/My-Workspace~768e55ed-ecbf-491b-88cb-a92754d74ccf/request/7899277-41221dfb-0dcc-49c9-90b5-4abe89b9b0fb?action=share&source=copy-link&creator=7899277&active-environment=fefbbb19-4ee0-493f-aa17-1a8f397e41fd). Make sure you have registered an account and logged in first
