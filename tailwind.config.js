/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors: {
        primary: '#161A30',
        secondary: '#31304D',
        tertiary: '#B6BBC4',
        fourtiary: '#F0ECE5',
        disabled: '#ACACAC',
        success: '#56E413',
        danger: '#FF0000',
        warning: '#EAD410',
      },
      boxShadow: {
        elevation1: '0 1px 2px 0 rgba(0, 0, 0, 30%), 0 1px 2px 1px rgba(0, 0, 0, 15%)',
        unset: 'unset'
      },
      fontSize: {
        'f12-l18': ['12px', '18px'], // font size = 12px, line height = 18px
        'f16-l24': ['16px', '24px'], // font size = 16px, line height = 24px
        'f20-l30': ['20px', '30px'] // font size = 20px, line height = 30px
      },
      borderWidth: {
        '1px': '1px'
      },
      borderRadius: {
        '8px': '8px'
      },
      transitionTimingFunction: {
        unset: 'unset'
      },
      transitionDuration: {
        unset: 'unset'
      },
      zIndex: {
        unset: 'unset'
      },
      spacing: {
        '1px': '1px',
        '5px': '5px',
        '8px': '8px',
        '10px': '10px',
        '15px': '15px',
        '18px': '18px',
        '20px': '20px',
        '24px': '24px',
        '30px': '30px',
        '44px': '44px',
        '48px': '48px',
        '64px': '64px',
        '65px': '65px',
        '66px': '66px',
        '84px': '84px',
        '100px': '100px',
        '150px': '150px',
        '153px': '153px',
        '320px': '320px',
        '340px': '340px',
        '360px': '360px',
        '380px': '380px',
        '100vh': '100vh',
        '50%': '50%',
        '100%': '100%',
        unset: 'unset'
      },
      width: {
        '1px': '1px',
        '5px': '5px',
        '8px': '8px',
        '10px': '10px',
        '15px': '15px',
        '18px': '18px',
        '20px': '20px',
        '24px': '24px',
        '30px': '30px',
        '44px': '44px',
        '48px': '48px',
        '64px': '64px',
        '65px': '65px',
        '66px': '66px',
        '84px': '84px',
        '100px': '100px',
        '150px': '150px',
        '153px': '153px',
        '320px': '320px',
        '340px': '340px',
        '360px': '360px',
        '380px': '380px',
        '100vh': '100vh',
        '50%': '50%',
        '100%': '100%',
        unset: 'unset'
      },
      height: {
        '1px': '1px',
        '5px': '5px',
        '8px': '8px',
        '10px': '10px',
        '15px': '15px',
        '18px': '18px',
        '20px': '20px',
        '24px': '24px',
        '30px': '30px',
        '44px': '44px',
        '48px': '48px',
        '64px': '64px',
        '65px': '65px',
        '66px': '66px',
        '84px': '84px',
        '100px': '100px',
        '150px': '150px',
        '153px': '153px',
        '320px': '320px',
        '340px': '340px',
        '360px': '360px',
        '380px': '380px',
        '100vh': '100vh',
        '50%': '50%',
        '100%': '100%',
        unset: 'unset'
      },
      minWidth: (theme) => ({
        ...theme('width')
      }),
      maxWidth: (theme) => ({
        ...theme('width')
      }),
      minHeight: (theme) => ({
        ...theme('height')
      }),
      maxHeight: (theme) => ({
        ...theme('height')
      })
    },
  },
  plugins: [],
}

